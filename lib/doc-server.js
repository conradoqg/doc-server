var express = require('express');
var path = require('path');
var fs = require('fs');
var merge = require('merge');
var watcher = require('./middlewares/watcher.js');
var tinylr = require('tiny-lr');
var markdownMiddleware = require('./middlewares/markdown.js');
require('string.prototype.endswith');

module.exports = function (options) {
    var baseOptions = {
        // Main application options
        directory: process.cwd(),
        port: 8080,

        // Secondary application options
        consoleLog: true,
        consoleType: 'dev',
        respondErrorLog: true,
        reloadDelay: 100,
        handleMD: true,
        mdHandler: 'mdwiki',
        livereload: true,
        compress: true,
        static: {
            index: ['index.html', 'index.html', 'index.md']
        }
    };

    options = merge(baseOptions, options);

    // Create express
    var app = express();

    var createLog = function () {
        if (options.consoleLog) {
            return require('morgan')(options.consoleType);
        }
    };

    var createAuth = function () {
        if (options.auth) {
            return require('./middlewares/auth.js')(options.auth.username, options.auth.password);
        }
    };

    var createCompress = function () {
        if (options.compress) {
            return require('compression')();
        }
    };

    var createLivereload = function () {
        if (options.livereload) {
            var srv = new tinylr.Server(merge({
                app: app
            }, options.liveReloadServer));

            return function tinylr(req, res, next) {
                srv.handler(req, res, next);
            };
        }
    };

    var createConnectLivereload = function () {
        if (options.livereload) {
            return require('connect-livereload')(merge({
                port: options.port,
            }, options.liveReloadMiddleware));
        }
    };

    var createWatcher = function () {
        if (options.livereload) {
            return watcher.middleware({
                basePath: options.directory
            });
        }
    };

    var createFindIndex = function () {
        return require('./middlewares/findIndex.js')({
            index: options.static.index,
            directory: options.directory
        });
    };

    var createMDHandler = function () {
        if (options.handleMD) {
            if (options.mdHandler === 'mdwiki') {
                return require('./middlewares/mdwiki.js')();
            } else {
                return markdownMiddleware({
                    basePath: options.directory,
                    maxAge: options.maxAge,
                    setHeaders: options.static.setHeaders,
                    template: fs.readFileSync(path.resolve(__dirname, '../public/template.html'), 'utf-8') || '{content}',
                    marked: options.marked
                });
            }
        }
    };

    var createStatic = function () {
        return express.static(options.directory, options.static);
    };

    var createPublicStatic = function () {
        return express.static(path.resolve(__dirname, '../public/'));
    };

    var createErrorHandler = function () {
        return require('./middlewares/errorHandler.js')({
            respondError: options.respondErrorLog,
            logError: options.consoleLog
        });
    };

    var createMiddlewares = function () {
        var middlewares = [
            createLog,
            createAuth,
            createCompress,
            createLivereload,
            createConnectLivereload,
            createWatcher,
            createFindIndex,
            createMDHandler,
            createStatic,
            createPublicStatic,
            createErrorHandler
        ];

        return middlewares;
    };

    var useMiddlewares = function (middlewares) {
        middlewares.forEach(function (element) {
            var middleware = element();
            if (middleware) {
                app.use(middleware);
            }
        }, this);
    };

    var addDefaultRoutes = function () {
        app.use('/404.md', function (req, res) {
            res.send('# 404\n###This page does not exist!').end();
        });

        app.use('*', function (req, res) {
            var template404 = fs.existsSync(path.resolve(options.directory, '404.html')) ? fs.readFileSync(path.resolve(options.directory, '404.html'), 'utf-8') : '<!DOCTYPE HTML><html><head><meta charset="utf-8"><title>404 - This page does not exist </title><style>@import url(http://fonts.googleapis.com/css?family=Bree+Serif|Source+Sans+Pro:300,400);*{maring: 0;padding: 0;}body{font-family: "Source Sans Pro", sans-serif;background: #E0E0E0;color: #475973;}.bree-font{font-family: "Bree Serif", serif;}#content{margin: 0 auto;width: 960px;}.clearfix:after{content: ".";display: block;clear: both;visibility: hidden;line-height: 0;height: 0;}.clearfix{display: block;}#main-body{text-align: center;}.enormous-font{font-size: 10em;margin-bottom: 0em;}</style></head><body> <div id="content"> <div class="clearfix"></div><div id="main-body"> <p class="enormous-font bree-font"> 404 </p><p class="big-font"> This page does not exist! </p></div></div></body></html>';
            res.status(404).send(template404).end();
        });
    };

    var middlewares = createMiddlewares();
    useMiddlewares(middlewares);
    addDefaultRoutes();

    // start the server
    var server = app.listen(options.port, options.host, function () {
        if (options.livereload) {
            watcher.watch({
                basePath: options.directory,
                interval: options.reloadDelay,
                tinylr: tinylr
            });
        }
        if (options.consoleLog) {
            console.log('doc-server serving \'' + options.directory + '\' http://' + (options.host || 'any') + ':' + server.address().port);
        }
    });
    return server;
};
