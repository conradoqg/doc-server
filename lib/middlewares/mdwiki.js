var querystring = require('querystring');

module.exports = function () {
    return function (req, res, next) {
        var filePath = req._parsedUrl.pathname.substring(1);
        var query = querystring.parse(req._parsedUrl.query);
        if (filePath.toLowerCase().endsWith('.md') && !query.hasOwnProperty('noParserMD')) {
            res.redirect('/mdwiki.html#!' + req.url);
            return res.end();
        }
        next();
    };
};
