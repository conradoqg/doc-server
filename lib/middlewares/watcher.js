var merge = require('merge');
var watch = require('node-watch');
var util = require('../util.js');
var path = require('path');
var querystring = require('querystring');

module.exports = {
    relatedFilePaths: {},
    watch: function (options) {
        merge({
            basePath: process.cwd(),
            tinylr: null,
            interval: 100
        }, options);
        var self = this;
        watch(options.basePath, {
            persistent: true,
            recursive: true,
            interval: options.interval
        }, util.delay(options.interval, function (filePath) {
            var filePathRelative = path.resolve('/', path.relative(options.basePath, filePath));
            console.log('RELOAD ' + filePathRelative);
            if (self.relatedFilePaths[filePath]) {
                options.tinylr.changed(self.relatedFilePaths[filePath]);
                delete self.relatedFilePaths[filePath];
            }
            options.tinylr.changed(filePathRelative);
        }));
    },
    middleware: function (options) {
        var self = this;
        merge({
            basePath: process.cwd()
        }, options);
        return function (req, res, next) {
            var query = querystring.parse(req._parsedUrl.query);

            if (query.hasOwnProperty('relatedTo')) {
                var filePath = req._parsedUrl.pathname.substring(1);
                self.relatedFilePaths[filePath] = query['relatedTo'];
            }

            next();
        };
    }
};
