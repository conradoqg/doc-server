'user strict';

var merge = require('merge');

module.exports = function (options) {
    var baseOptions = {
        respondError: false,
        logError: true
    };

    options = merge(baseOptions, options);

    return function (err, req, res, next) {
        if (err) {
            var responseText = null;

            if (options.logError) {
                console.error(err.stack);
            }

            if (options.respondError) {
                responseText = err.stack;
            } else {
                responseText = 'Internal Server Error';
            }

            res.status(500).send(responseText).end();
        } else {
            next();
        }
    };
};
