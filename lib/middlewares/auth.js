module.exports = function (username, password) {
    var checkUser = function (credentials) {
        if (credentials.name === username && credentials.pass === password) {
            return true;
        } else {
            return false;
        }
    };

    var middleware = function (req, res, next) {
        var basicAuth = require('basic-auth');
        var authorized = false;

        var credentials = basicAuth(req);

        if (credentials) {
            authorized = checkUser(credentials);
        }

        if (authorized) {
            return next();
        } else {
            return respondUnauthorized(res);
        }
    };

    var respondUnauthorized = function (res) {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.sendStatus(401);
    };

    return middleware;
};
