var merge = require('merge');
var fs = require('fs');
var path = require('path');
var util = require('../util');

var findFirstIndexMatch = function (basePath, filePath, indexes) {
    var relativeFilePath = filePath.substring(1);
    var indexPath = null;

    for (var i in indexes) {
        if (fs.existsSync(path.resolve(basePath, relativeFilePath, indexes[i]))) {
            indexPath = filePath + indexes[i];
        }
    }

    return indexPath;
};

module.exports = function (options) {
    var baseOptions = {
        index: ['index.html', 'index.htm'],
        directory: process.cwd()
    };

    options = merge(baseOptions, options);
    options.index = util.arrayfy(options.index);

    var middleware = function (req, res, next) {
        var filePath = req._parsedUrl.pathname;
        var redirectFile = null;

        if (filePath.endsWith('/')) {
            redirectFile = findFirstIndexMatch(options.directory, filePath, options.index);
        }

        if (redirectFile) {
            res.redirect(redirectFile);
            return res.end();
        } else {
            return next();
        }
    };
    return middleware;
};
