var fs = require('fs');

module.exports = function (options) {
    var merge = require('merge');
    var querystring = require('querystring');

    options = merge({
        basePath: process.cwd(),
        maxAge: 3600,
        template: '<body>{content}</body>',
        marked: {}
    }, options);

    return function (req, res, next) {
        var path = require('path');

        var filePath = req._parsedUrl.pathname.substring(1);
        var query = querystring.parse(req._parsedUrl.query);
        if (filePath.toLowerCase().endsWith('.md') && !query.hasOwnProperty('noParserMD')) {
            filePath = path.resolve(options.basePath, filePath);
            fs.exists(filePath, function (exists) {
                var respondMarkedContent = function(status, content) {
                    var marked = require('color-marked');
                    var html = marked(content, merge(options.marked, {
                        color: true
                    }));
                    if (options.setHeaders) {
                        options.setHeaders(res);
                    }
                    res.writeHead(status, {
                        'Content-type': 'text/html; charset=utf-8',
                        'Cache-Control': 'public, max-age=' + options.maxAge + '\''
                    });

                    res.end(options.template.replace(/\$\{content\}/, html));
                };

                if (!exists) {
                    return respondMarkedContent(404, '# 404\n###This page does not exist!');
                }
                fs.readFile(filePath, 'utf-8', function (err, text) {
                    if (err) {
                        return next(err);
                    }

                    return respondMarkedContent(200, text);
                });
            });
        } else {
            next();
        }
    };
};
