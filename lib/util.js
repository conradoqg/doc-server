module.exports = {
    delay: function (ms, fn) {
        var timer;
        return function () {
            var args = arguments;
            if (timer) {
                clearTimeout(timer);
            }
            timer = setTimeout(function () {
                fn.apply(null, args);
            }, ms);
        };
    },
    arrayfy: function (value) {
        if (!(value instanceof Array)) {
            value = [value];
        }

        return value;
    }
};
