'use strict';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var watching = false;

var paths = {
    lint: ['./gulpfile.js', './lib/**/*.js', './test/**/*.js'],
    watch: ['./gulpfile.js', './lib/**', './test/**/*.js', '!test/{temp,temp/**}'],
    tests: ['./test/**/*.js', '!test/{temp,temp/**}'],
    source: ['./lib/**/*.js'],
    report: './report'
};

var plumberConf = {};

if (process.env.CI) {
    plumberConf.errorHandler = function (err) {
        throw err;
    };
}

/**
 * Test.
 */

gulp.task('lint', function () {
    return gulp.src(paths.lint)
        .pipe(plugins.jshint('.jshintrc'))
        .pipe(plugins.plumber(plumberConf))
        .pipe(plugins.jscs())
        .pipe(plugins.jshint.reporter('jshint-stylish'));
});

function onError(err) {
    if (watching) {
        /*jshint validthis:true */
        this.emit('end');
    } else {
        if (err) {
            console.log(err.toString());
        }
    }
}

gulp.task('istanbul', function (cb) {
    gulp.src(paths.source)
        .pipe(plugins.istanbul({
            includeUntested: true
        })) // Covering files
        .pipe(plugins.istanbul.hookRequire())
        .on('finish', function () {
            gulp.src(paths.tests)
                .pipe(plugins.plumber(plumberConf))
                .pipe(plugins.mocha().on('error', onError))
                .pipe(plugins.istanbul.writeReports({
                    reporters: ['text', 'text-summary']
                }))
                .on('end', cb);
        });
});

gulp.task('complexity', function () {
    return gulp.src(paths.lint)
        .pipe(plugins.complexity({
            cyclomatic: [4, 8, 13],
            halstead: [10, 15, 22],
            maintainability: 100,
            breakOnErrors: false,
        }));
});

/**
 * Release
 */

gulp.task('bump:patch', ['test'], function () {
    return gulp.src(['./package.json'])
        .pipe(plugins.bump({ type: 'patch' }))
        .pipe(gulp.dest('./'));
});
gulp.task('bump:minor', ['test'], function () {
    return gulp.src(['./package.json'])
        .pipe(plugins.bump({ type: 'patch' }))
        .pipe(gulp.dest('./'));
});
gulp.task('bump:major', ['test'], function () {
    return gulp.src(['./package.json'])
        .pipe(plugins.bump({ type: 'patch' }))
        .pipe(gulp.dest('./'));
});

/**
 * Test-watch
 */
gulp.task('set-watching', function () {
    watching = true;
});

gulp.task('test:watch', ['set-watching', 'test'], function () {
    gulp.watch(paths.watch, ['test']);
});

/**
 * Security
 */

//To check your package.json
gulp.task('nsp', function (cb) {
    plugins.gulpNSP({
        path: './package.json',
        stopOnError: false
    }, cb);
});

/*
//To check your shrinkwrap.json
gulp.task('nsp', function (cb) {
  gulpNSP('./npm-shrinkwrap.json', cb);
});

//If you don't want to stop your gulp flow if some vulnerabilities have been found:
gulp.task('nsp', function (cb) {
  gulpNSP({
    path: './npm-shrinkwrap.json',
    stopOnError: false
  }, cb);
});
*/

/**
 * Reports
 */

gulp.task('plato', ['report:clear'], function () {
    return gulp.src(paths.source)
        .pipe(plugins.plato(paths.report + '/plato', {
            jshint: {
                options: {
                    strict: true
                }
            },
            complexity: {
                trycatch: true
            }
        }));
});

gulp.task('js-doc', ['report:clear'], function (cb) {
    plugins.run('node node_modules/jsdoc/jsdoc lib -R README.MD -P package.json -r -d ' + paths.report + '/doc').exec(cb);
});

gulp.task('report:istanbul', ['report:clear'], function (cb) {
    gulp.src(paths.source)
        .pipe(plugins.istanbul({
            includeUntested: true
        })) // Covering files
        .pipe(plugins.istanbul.hookRequire()) // Force `require` to return covered files
        .on('finish', function () {
            gulp.src(paths.tests)
                .pipe(plugins.plumber(plumberConf))
                .pipe(plugins.mocha())
                .pipe(plugins.istanbul.writeReports({
                    dir: paths.report + '/istanbul'
                })) // Creating the reports after tests runned
                .on('finish', function () {
                    process.chdir(__dirname);
                    cb();
                });
        });
});

/**
 * Utils
 */

gulp.task('size', function () {
    return gulp.src(paths.source)
        .pipe(plugins.size());
});

gulp.task('report:clear', function (cb) {
    var del = require('del');
    del(paths.report, cb);
});

gulp.task('test', ['lint', 'istanbul', 'complexity', 'size']);

gulp.task('report', ['plato', 'js-doc', 'report:istanbul']);

gulp.task('security-check', ['nsp']);

gulp.task('default', ['test']);
