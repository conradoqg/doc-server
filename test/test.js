/* global should */
'use strict';

var path = require('path');
process.env.NODE_PATH = path.resolve(__dirname, '../');
require('module').Module._initPaths();

global.should = require('should').noConflict();
should.extend();
