'use strict';

var docServer = require('../../lib/doc-server.js');
var supertest = require('supertest');

describe('Doc Server', function () {

    describe('base features', function () {
        it('should start', function (done) {
            var app = docServer({
                port: 0,
                livereload: false,
                consoleLog: false
            });
            supertest(app)
                .get('/')
                .expect(404, function (err) {
                    app.close(done(err));
                });
        });

        it('should respond with 404 template', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1/',
                livereload: false,
                consoleLog: false
            });
            supertest(app)
                .get('/notExist.html')
                .expect(404)
                .expect(/<title>404 - This page does not exist <\/title>/, function (err) {
                    app.close(done(err));
                });
        });

        it('should respond with 404 file', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/2/',
                livereload: false,
                consoleLog: false
            });
            supertest(app)
                .get('/notExist.html')
                .expect(404)
                .expect('404 HTML', function (err) {
                    app.close(done(err));
                });
        });

        it('should respond with 404 markdown file', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/2/',
                livereload: false,
                consoleLog: false
            });
            supertest(app)
                .get('/404.md?noParserMD')
                .expect(200)
                .expect('# 404\n###This page does not exist!', function (err) {
                    app.close(done(err));
                });
        });

        it('should not parse markdown file', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/2/',
                handleMD: false,
                livereload: false,
                consoleLog: false
            });
            supertest(app)
                .get('/404.md')
                .expect(200)
                .expect('# 404\n###This page does not exist!', function (err) {
                    app.close(done(err));
                });
        });
    });

    describe('authentication middleware', function () {
        it('should start with authentication and be authenticated', function (done) {
            var app = docServer({
                port: 0,
                livereload: false,
                auth: {
                    username: 'testName',
                    password: 'testPass'
                },
                consoleLog: false
            });
            supertest(app)
                .get('/')
                .auth('testName', 'testPass')
                .expect(404, function (err) {
                    app.close(done(err));
                });
        });

        it('should start with authentication and be unauthorized', function (done) {
            var app = docServer({
                port: 0,
                livereload: false,
                auth: {
                    username: 'testName',
                    password: 'testPass'
                },
                consoleLog: false
            });
            supertest(app)
                .get('/')
                .auth('wrongName', 'wrongPass')
                .expect(401, function (err) {
                    app.close(done(err));
                });
        });

        it('should start with authentication and not be authenticated', function (done) {
            var app = docServer({
                port: 0,
                livereload: false,
                auth: {
                    username: 'testName',
                    password: 'testPass'
                },
                consoleLog: false
            });
            supertest(app)
                .get('/')
                .expect(401, function (err) {
                    app.close(done(err));
                });
        });
    });

    describe('markdown middleware', function () {
        it('should use markdown middleware', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1/',
                livereload: false,
                mdHandler: 'marked',
                consoleLog: false
            });
            supertest(app)
                .get('/test.md')
                .expect(200)
                .expect(/<h1 id="title">Title<\/h1>/, function (err) {
                    app.close(done(err));
                });
        });

        it('should return 404 markdown page', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1/',
                livereload: false,
                mdHandler: 'marked',
                consoleLog: false
            });
            supertest(app)
                .get('/404.md')
                .expect(404)
                .expect(/<h1 id="404">404<\/h1>/, function (err) {
                    app.close(done(err));
                });
        });

        it('should set additional header to markdown', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1/',
                livereload: false,
                static: {
                    setHeaders: function (res) {
                        res.set('headerName', 'headerValue');
                    }
                },
                mdHandler: 'marked',
                consoleLog: false
            });
            supertest(app)
                .get('/test.md')
                .expect(200)
                .expect('headerName', 'headerValue')
                .expect(/<h1 id="title">Title<\/h1>/, function (err) {
                    app.close(done(err));
                });
        });

        it('should not find markdown file', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1',
                livereload: false,
                mdHandler: 'marked',
                consoleLog: false
            });
            supertest(app)
                .get('/notExist.md')
                .expect(404, function (err) {
                    app.close(done(err));
                });
        });

        it('should not read markdown file', function (done) {
            var rewire = require('rewire');
            var errno = require('errno');

            var createSimulatedError = function (filename) {
                var simulatedSignature = errno.code['ENOENT'];
                var simulatedError = new Error(simulatedSignature.description);
                simulatedError.code = simulatedSignature.code;
                simulatedError.errno = simulatedSignature.errno;
                simulatedError.path = filename;
                return simulatedError;
            };

            //var fsMock = require('fs');
            //var realReadFile = fsMock.readFile;
            var fsMock = require('fs');
            fsMock._exists = fsMock.exists;
            fsMock._readFile = fsMock.readFile;
            fsMock.exists = function (path, callback) {
                if (path && path.indexOf('unreadableFile.md') >= -1) {
                    return callback(true);
                }
                return this._exists(path, callback);
            };
            fsMock.readFile = function (filename, options, callback) {
                callback = callback || options;
                if (filename && filename.indexOf('unreadableFile.md') >= -1) {
                    if (callback) {
                        return callback(createSimulatedError(filename));
                    }
                }
                return this._readFile.apply(this, arguments);
            };

            var mockedMarkdownMiddleware = rewire('../../lib/middlewares/markdown.js');
            mockedMarkdownMiddleware.__set__('fs', fsMock);
            var mockedDocServer = rewire('../../lib/doc-server.js');
            mockedDocServer.__set__('markdownMiddleware', mockedMarkdownMiddleware);

            var app = mockedDocServer({
                port: 0,
                directory: 'test/fixtures/1',
                livereload: false,
                mdHandler: 'marked',
                consoleLog: false,
                respondErrorLog: false
            });
            supertest(app)
                .get('/unreadableFile.md')
                .expect(500, function (err) {
                    app.close(done(err));
                });
        });

        it('should not parse markdown file', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1',
                livereload: false,
                consoleLog: false,
                mdHandler: 'marked'
            });
            supertest(app)
                .get('/test.md')
                .query('noParserMD')
                .expect(200)
                .expect('# Title', function (err) {
                    app.close(done(err));
                });
        });
    });

    describe('mdwiki middleware', function () {
        it('should use mdwiki middleware', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1/',
                livereload: false,
                consoleLog: false
            });
            supertest(app)
                .get('/test.md')
                .expect(302)
                .expect('Moved Temporarily. Redirecting to /mdwiki.html#!/test.md', function (err) {
                    app.close(done(err));
                });
        });

        it('should return 302 and a redirect to mkdwiki page', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1/',
                livereload: false,
                consoleLog: false
            });
            supertest(app)
                .get('/404.md')
                .expect(302)
                .expect('Moved Temporarily. Redirecting to /mdwiki.html#!/404.md', function (err) {
                    app.close(done(err));
                });
        });
    });

    describe('findIndex middleware', function () {
        it('should use an alternative index file', function (done) {
            var app = docServer({
                port: 0,
                directory: 'test/fixtures/1/',
                livereload: false,
                static: {
                    index: 'test.md'
                },
                consoleLog: false
            });
            supertest(app)
                .get('/')
                .expect(302)
                .expect('Moved Temporarily. Redirecting to /test.md', function (err) {
                    app.close(done(err));
                });
        });
    });
    /*
     describe('errorHandler middleware', function () {
        it('should log to console', function (done) {

        });
     });
     */
});
