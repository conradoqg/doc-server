'use strict';

var util = require('../../lib/util.js');

describe('Util', function () {
    it('should delay 200ms', function (done) {
        var startTime = process.hrtime();
        util.delay(200, function () {
            var endTime = process.hrtime(startTime);
            var elapsedTime = (endTime[0] * 1000) + (endTime[1] / 1000000);
            elapsedTime.should.be.above(195, 'The delay took less than 200ms : ' + elapsedTime);
            done();
        })();
    });

    it('should delay 200ms even if it is called twice', function (done) {
        var startTime = process.hrtime();
        var called = 0;

        var testFunction = function () {
            var endTime = process.hrtime(startTime);
            var elapsedTime = (endTime[0] * 1000) + (endTime[1] / 1000000);
            called++;
            elapsedTime.should.be.above(195, 'The delay took less than 200ms : ' + elapsedTime);
            called.should.be.equal(1, 'The delay was called twice in less than 200ms.');
            done();
        };

        var delayedFunction = util.delay(200, testFunction);
        delayedFunction();
        delayedFunction();
    });

    it('should arrayfy a value different of an array', function (done) {
        var subject = 'String';
        util.arrayfy(subject).should.be.instanceof(Array).and.have.lengthOf(1);
        done();
    });

    it('should not arrayfy an array', function (done) {
        var subject = [1,2,3];
        util.arrayfy(subject).should.be.instanceof(Array).and.have.lengthOf(3);
        done();
    });
});
