#!/usr/bin/env node

var pkg = require('./../package.json');
var docServer = require('./../lib/doc-server.js');
var dashdash = require('dashdash');
var fs = require('fs');

var cliOptions = [{
    group: 'Server Options'
}, {
        names: ['port', 'p'],
        type: 'integer',
        default: 8080,
        help: 'TCP port at which the files will be served. (Default: 8080)'
    }, {
        names: ['host-addresss', 'a'],
        type: 'string',
        helpArg: '127.0.0.1',
        help: 'Begin accepting connections on the specified host. If the host is omitted, the server will accept connections directed to any IPv4 address. (Default: omitted)'
    }, {
        names: ['no-log', 'l'],
        type: 'bool',
        default: false,
        help: 'Disable logging requests and file reloads in console. (Default: enable)'
    }, {
        names: ['log-type', 't'],
        type: 'string',
        default: 'dev',
        helpArg: 'TYPE',
        help: 'Log format. Possible values are "combined", "common", "dev", "short" or "tiny". (Default: dev)'
    }, {
        names: ['no-respond-error', 'e'],
        type: 'bool',
        default: false,
        help: 'Disable respond error and stack-trace back to requester. (Default: enable)'
    }, {
        names: ['reload-delay', 'r'],
        type: 'integer',
        default: 100,
        help: 'Interval in ms, which the server monitors the file system and reload webpage. (Default: 100)'
    }, {
        names: ['no-parser-md', 'd'],
        type: 'bool',
        default: false,
        help: 'Disable markdown parser. (Default: Enabled)'
    }, {
        names: ['md-type', 'y'],
        type: 'string',
        default: 'mdwiki',
        helpArg: 'TYPE',
        help: 'Markdown handler to use. Possible value are "mdwiki" and "marked". (Default: mdwiki)'
    }, {
        names: ['no-livereload', 'c'],
        type: 'bool',
        default: false,
        help: 'Disable live reload. (Default: Enabled)'
    }, {
        names: ['max-age', 'm'],
        type: 'integer',
        default: 0,
        help: 'Set the max-age property of the Cache-Control header in milliseconds or a string in ms format. (Default: 0)'
    }, {
        names: ['headers-file'],
        type: 'string',
        help: 'JSON file of additional headers.',
        helpArg: 'headers.json'
    }, {
        names: ['no-compress', 'g'],
        type: 'bool',
        default: false,
        help: 'Disable compression. (Default: enable)'
    }, {
        group: 'Authentication'
    }, {
        names: ['username', 'u'],
        type: 'string',
        helpArg: 'user',
        help: 'Username to authenticate.'
    }, {
        names: ['password', 'w'],
        type: 'string',
        helpArg: 'pass',
        help: 'Password to authenticate.'
    }, {
        group: 'Static Options'
    }, {
        names: ['static-dotFile'],
        type: 'string',
        default: 'ignore',
        help: 'Option for serving dotfiles. Possible values are “allow”, “deny”, and “ignore”. (Default: ignore)'
    }, {
        names: ['static-no-etag'],
        type: 'bool',
        default: false,
        help: 'Disable etag generation. (Default: enable)'
    }, {
        names: ['static-extension'],
        type: 'arrayOfString',
        default: [],
        helpArg: '.ext',
        help: 'Sets file extension fallbacks. Repeat --static-extension for each extension. (Default: empty)'
    }, {
        names: ['static-no-indexing'],
        type: 'bool',
        default: false,
        help: 'Disable directory indexing. (Default: enable)'
    }, {
        names: ['static-index', 'i'],
        type: 'arrayOfString',
        default: ['index.html', 'index.html', 'index.md'],
        helpArg: 'file.ext',
        help: 'If directory indexing, sends directory index file. Repeat --static-extension for each extension. (Default: index.html, index.htm, index.md)'
    }, {
        names: ['static-no-last-modified'],
        type: 'bool',
        default: false,
        help: 'Desable the Last-Modified header to the last modified date of the file on the OS. (Default: enable)'
    }, {
        names: ['static-no-redirect'],
        type: 'bool',
        default: false,
        help: 'Disable redirect to trailing “/” when the pathname is a directory. (Default: enable)'
    }, {
        group: 'Live Reload Options',
    }, {
        names: ['lr-no-css'],
        type: 'bool',
        default: false,
        help: 'Disable LiveReload live CSS reloading (Default: enable)'
    }, {
        names: ['lr-no-js'],
        type: 'bool',
        default: false,
        help: 'Disable LiveReload live JS reloading (Default: enable)'
    }, {
        names: ['lr-no-img'],
        type: 'bool',
        default: false,
        help: 'Disable LiveReload live IMG reloading (Default: enable)'
    }, {
        names: ['lr-ignore'],
        type: 'arrayOfString',
        default: null,
        helpArg: '.ext',
        help: 'Sets file extension to ignore. Repeat --lr-ignore for each extension.'
    }, {
        names: ['lr-include'],
        type: 'arrayOfString',
        default: null,
        helpArg: '.ext',
        help: 'Sets file extension to include. Repeat --lr-include for each extension.'
    }, {
        group: 'MD Wiki Options: (see http://mdwiki.info for details)'
    }, {
        group: 'Marked Options'
    }, {
        names: ['md-no-gfm'],
        type: 'bool',
        default: false,
        help: 'Disable GitHub flavored markdown. (Default: enable)'
    }, {
        names: ['md-no-table'],
        type: 'bool',
        default: false,
        help: 'Disable GFM tables. This option requires the gfm option to be true. (Default: enable)'
    }, {
        names: ['md-breaks'],
        type: 'bool',
        default: false,
        help: 'Enable GFM line breaks. This option requires the gfm option to be true. (Default: disable)'
    }, {
        names: ['md-pedantic'],
        type: 'bool',
        default: false,
        help: 'Conform to obscure parts of markdown.pl as much as possible. Don\'t fix any of the original markdown bugs or poor behavior. (Default: disable)'
    }, {
        names: ['md-sanitize'],
        type: 'bool',
        default: false,
        help: 'Sanitize the output. Ignore any HTML that has been input. (Default: disable)'
    }, {
        names: ['md-no-smartlists'],
        type: 'bool',
        default: false,
        help: 'Disable the smarter list behavior than the original markdown. May eventually be default with the old behavior moved into pedantic. (Default: enable)'
    }, {
        names: ['md-smartypants'],
        type: 'bool',
        default: false,
        help: 'Use "smart" typograhic punctuation for things like quotes and dashes. (Default: disable)'
    }, {
        group: 'General Options'
    }, {
        names: ['version', 'v'],
        type: 'bool',
        help: 'doc-server version.'
    }, {
        // `names` or a single `name`. First element is the `opts.KEY`.
        names: ['help', 'h'],
        // See "Option specs" below for types.
        type: 'bool',
        help: 'Print this help and exit.'
    }];

var parser = dashdash.createParser({
    options: cliOptions
});

var showHelp = function showHelp(exitCode, message) {
    try {
        var help = parser.help({
            includeEnv: true,
            helpWrap: false
        }).trimRight();
        help = help +
        '\n\n' +
        '  URL Integration:\n\n' +
        '    Use ?relatedTo=<file> in <a href=\'<URL>\'> to tell what monitored file will update the URL path. (Example: http://127.0.0.1/index.md?relatedTo=user.md which will tell to update the index.md if user.md is modified).\n' +
        '    Use ?noParserMD in URL to disable markdown parse. (Example: http://127.0.0.1/index.md?noParserMD).\n'
    }
    catch (e) {

    }

    message = 'usage: doc-server [OPTIONS] [directory]\n\n' + 'Options:\n' + help + (message ? '\n\n' + message : '')

    if (exitCode >= 1)
        console.error(message);
    else if (exitCode === 0)
        console.log(message);

    if (exitCode !== null) process.exit(exitCode);
}

try {
    var cliArgv = parser.parse(process.argv);

    // Help
    if (cliArgv.help) {
        showHelp(0)
    }

    // Version
    if (cliArgv.version) {
        console.log('doc-server', pkg.version);
        process.exit(0);
    }

    var options = {};

    // Translate options
    options.directory = (cliArgv._args.length == 0) ? '.' : cliArgv._args[0];
    options.port = cliArgv.port;
    options.host = cliArgv.host_addresss;
    options.consoleLog = !cliArgv.no_log;
    options.consoleType = cliArgv.log_type;
    options.respondErrorLog = !cliArgv.no_respond_error;
    options.reloadDelay = cliArgv.reload_delay;
    options.handleMD = !cliArgv.no_parser_md;
    options.liveReload = !cliArgv.no_livereload;
    options.maxAge = cliArgv.max_age;
    options.compress = !cliArgv.no_compress;
    options.mdHandler = cliArgv.md_type;
    if (cliArgv.username && cliArgv.password) {
        options.auth = {
            username: cliArgv.username,
            password: cliArgv.password
        }
    }
    options.static = {};
    options.static.dotFile = cliArgv.static_dotFile;
    options.static.etag = !cliArgv.static_no_etag;
    options.static.extensions = cliArgv.static_extension;
    if (!cliArgv.static_no_indexing) options.static.index = cliArgv.static_index;
    else options.static.index = false;
    options.static.lastModified = !cliArgv.static_no_last_modified;
    options.static.maxAge = cliArgv.max_age;
    options.static.redirect = !cliArgv.static_no_redirect;
    if (cliArgv.headers_file) {
        if (fs.existsSync(cliArgv.headers_file)) {
            try {
                var headers = JSON.parse(fs.readFileSync(cliArgv.headers_file, 'utf8'));
                if (headers) {
                    options.static.setHeaders = function (res) {
                        for (var headerKey in headers) {
                            res.set(headerKey, headers[headerKey]);
                        }
                    }
                }
                else {
                    showHelp(1, 'File "' + cliArgv.headers_file + '" is not in JSON format!');
                }
            }
            catch (e) {
                showHelp(1, 'File "' + cliArgv.headers_file + '" is not in JSON format!');
            }
        }
        else {
            showHelp(1, 'File "' + cliArgv.headers_file + '" not found!');
        }
    }
    else {
        options.static.setHeaders = function (res) { };
    }
    options.liveReloadServer = {};
    options.liveReloadServer.liveCSS = !cliArgv.lr_no_css;
    options.liveReloadServer.liveJS = !cliArgv.lr_no_js;
    options.liveReloadServer.liveImg = !cliArgv.lr_no_img;
    options.liveReloadMiddleware = {};
    options.liveReloadMiddleware.ignore = cliArgv.lr_ignore;
    options.liveReloadMiddleware.include = cliArgv.lr_include;
    options.marked = {};
    options.marked.gfm = !cliArgv.md_no_gfm;
    options.marked.table = !cliArgv.md_no_table;
    options.marked.breaks = cliArgv.md_breaks;
    options.marked.pedantic = cliArgv.md_pedantic;
    options.marked.sanitize = cliArgv.md_sanitize;
    options.marked.smartLists = !cliArgv.md_no_smartlists;
    options.marked.smartypants = cliArgv.md_smartypants;

    // Run doc-server
    docServer(options);
}
catch (e) {
    showHelp(1, 'error: ' + e.message + '\n' + e.stack);
    process.exit(1);
}